


///////////////////////////////
// HTML5 input tag
var Input = Backbone.Model.extend({
  defaults: {
    type: 'text',
    required: false
  },

  initialize: function() {
    ///// IF NECESSARY CHECK & PREPARE VARIABLES

    // prepare placeholer
    if( typeof(this.get('placeholder'))!=='undefined' ) {
      this.set('placeholder', 'placeholder="'+ this.get('placeholder') +'"');
    } else {
      this.set('placeholder', '');    
    }

    // prepare disabled
    if( typeof(this.get('disabled'))!=='undefined' ) {
      this.set('disabled', 'disabled');
    } else {
      this.set('disabled', '');    
    }

    // prepare value
    if( typeof(this.get('value'))!=='undefined' ) {
      this.set('value', 'value="'+ this.get('value') +'"');
    } else {
      this.set('value', '');
    }
  },

  przelicz: function() {
    console.log('przeliczam...');
  }
});


///////////////////////////////////
var InputView = Backbone.View.extend({
  template: _.template($('#input-template').html()),

  initialize: function() {
    var input = this;

    this
      .render()
      // .$el
      // .find('input')
      //   .keyup(function() { 
      //     input.model.przelicz();
      //   });
  },

  render: function() {
    this.$el.html(this.template( this.model.toJSON() ));

    if(  typeof(this.model.get('class')) !== 'undefined'  ) {
      this.$el.find('#'+this.model.get('id'))
        .addClass(this.model.get('class'));
    }

    return this; // just for chainig
  }
});


// Fieldset View
var FieldsetView = Backbone.View.extend({

  template: _.template($('#fieldset-template').html()),

  initialize: function() {
    this.render();
  },

  render: function() {
    this.$el.html(this.template(this.options));
  }
});



///////////////////////////////////////////
// Application
var App = Backbone.View.extend({
  el: $('#body'),

  title: 'Kwiz 1.0',

  initialize: function() {
    this.render();
  },

  render: function() {
    var cenaOferowana = new Input({
      label: 'Cena oferowana',
      id: 'cena-oferowana',
      class: 'input-small',
      required: true,
      placeholder: 'Cena oferowana',
      value: 234000,
      append: 'zł'
    });

    var cenaWynajmu = new Input({
      label: 'Cena wynajmu',
      id: 'cena-wynajmu',
      class: 'input-small',
      placeholder: 'Cena wynajmu',
      value: 1700,
      append: 'zł / miesiąc'
    });

    this.$el.append(new FieldsetView({legend: 'Dane'}).el);

    var coView = new InputView({model: cenaOferowana});
    coView.$el.find('input').keyup(this.recalculate);
    this.$el.append(coView.el);

    var cwView = new InputView({model: cenaWynajmu});
    cwView.$el.find('input').keyup(this.recalculate);
    this.$el.append(cwView.el);

    this.$el.append(new FieldsetView({legend: 'Wyniki'}).el);

    this.$el.append(new InputView({model: new Input({
      label: 'Zwrot z inwestycji',
      id: 'zwrot-inwestycji',
      class: 'input-small',
      append: '%',
      placeholder: '%',
      disabled: true,
    })}).el);

    this.$el.append(new InputView({model: new Input({
      label: 'Zwrot z inwestycji',
      id: 'zwrot-inwestycji2',
      class: 'input-small',
      append: 'lat',
      placeholder: 'lat',
      disabled: true
    })}).el);

  },

  allDataAreCorrect: function() {
    if(
      $.isNumeric(app.$el.find('#cena-oferowana').val()) &&
      $.isNumeric(app.$el.find('#cena-wynajmu').val())
    ) {
      return true;  
    } else {
      return false;
    }
  },

  recalculate: function() {
    // console.log(app);
    if(app.allDataAreCorrect()) {
      // dane
      var cOferowana = app.$el.find('#cena-oferowana').val();
      var cWynajmu = app.$el.find('#cena-wynajmu').val();

      ////////////////////////
      // obliczenia
      // ((cena wynajmu * 12mies) / cena oferowana ) * 100
      procenty = ((cWynajmu*12) / cOferowana) * 100;      

      // cena oferowana / (cena wynajmu * 12mies) 
      lata = cOferowana / (cWynajmu*12);

      ///////////////////////
      // wyniki
      app.$el.find('#zwrot-inwestycji').val(procenty.toFixed(2).replace('.', ','));
      app.$el.find('#zwrot-inwestycji2').val(lata.toFixed(2).replace('.', ','));
    }
  }

});


///////////////////// RUN
// kickoff final APPLICATION
var app = new App;

// first time recalculation
app.recalculate();